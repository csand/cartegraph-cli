import { http, api } from "./http";

export class DiscoverRecordsetResponse {
  SingularName: string;
  PluralName: string;
  ClassName: string;
}

export class DiscoverCategoryResponse {
  Library: DiscoverRecordsetResponse[];
  Asset: DiscoverRecordsetResponse[];
  Resource: DiscoverRecordsetResponse[];
  Request: DiscoverRecordsetResponse[];
  Work: DiscoverRecordsetResponse[];
}

export let discover: DiscoverCategoryResponse;

export const recordsets = async () => {
  discover =
    discover || (await api().get<DiscoverCategoryResponse>("discover")).data;
  return [
    ...discover.Asset,
    ...discover.Request,
    ...discover.Work,
    ...discover.Resource
  ];
};

export const findRecordset = async (s: string) => {
  const r = await recordsets();
  return r.find(
    x =>
      x.ClassName.toLowerCase() === s.toLowerCase() ||
      x.PluralName.toLowerCase() === s.toLowerCase() ||
      x.SingularName.toLowerCase() === s.toLowerCase()
  );
};
