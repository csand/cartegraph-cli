import { ClassMeta } from "./meta";
import { api } from "./http";
export enum eFieldType {
  Undefined = -1,
  Text = 0,
  YN = 1,
  Integer = 2,
  FloatingPoint = 3,
  Currency = 4,
  DateTime = 5,
  LookUp = 6,
  Attachment = 9,
  Quantity = 10,
  Date = 11,
  Time = 12,
  Memo = 13,
  QuantityUnit = 14,
  TextLookUp = 15,
  FilterText = 16,
  FilterXML = 17
}

export interface PropertyMeta {
  OID: number;
  Name: string;
  Uid: string;
  LookupField: string;
  LookupClass?: string;
  IsLookUpAuthorized: boolean;
  IsRefEnforcedLookup: boolean;
  IsRequired: boolean;
  IsReadOnly: boolean;
  IsSystem: boolean;
  IsUnique: boolean;
  IsDynamicLookup: boolean;
  HasInitialValue: boolean;
  FieldType: number;
  DisplayName: string;
  FieldLength: number;
  FieldSize: number;
  OCIWeight?: number;
  IsAutoNumber: boolean;
  ParentClassName: string;
  UnitCategory?: string;
  DefaultUnit?: string;
  IsMemo: boolean;
  IsContainerLookup: boolean;
}

export interface ClassMeta {
  OID: number;
  Name: string;
  Uid: string;
  DefaultSort: string;
  PrimaryField: string;
  PluralName: string;
  GeoType: number;
  OCIWeight?: number;
  BOType: number;
  IsAuthorized: boolean;
  IsReadOnly: boolean;
  IsComponentSet: boolean;
  CachedColor: number;
  TrueClassForShare?: string;
  IsShared: boolean;
  ShareBindingField?: string;
  Children: string[];
  Fields: PropertyMetaCollection;
  SingularName: string;
  CachedGisInfo?: any;
  TrueParentForShare?: string;
  ParentClass?: string;
  ShortUID: string;
  TopMostParent: string;
  HierarchicalDepth: number;
  LinkableAssetRelationships?: any;
  LinkableAssetClasses?: any;
  ContainerComponentMultipleLookupRelationships?: any[];
}

export interface PropertyMetaCollection {
  [fieldName: string]: PropertyMeta;
}

export interface ClassMetaResponse {
  [className: string]: ClassMeta;
}

/* tslint:disable: variable-name */

export interface FlexView {
  Oid: number;
  ID: string;
  ViewType: number;
  ViewName: string;
  Roles: string;
  Sections: Section[];
  Pages: any[];
  ViewDenied: boolean;
}

/**
 * What type of UI control is this. E.g. a field, grid, etc.
 */
export type FlexViewItemType =
  | "field"
  | "grid"
  | "group-header"
  | "task-assignments"
  | "field-through-lookup"
  | "";

export class Item {
  ColumnWidth?: string;
  AltSortField?: string;
  AttachmentUid?: string;
  FrozenColumn?: string;
  ID: string;
  Name: string;
  UID: string;
  Height?: string;
  GridStyle?: string;
  GridFilter?: string;
  InfoLookup?: string;
  Items?: Item[];
  HideFrozenColumn?: boolean;
  ItemType?: FlexViewItemType;
  FieldsThroughLookup?: string[];

  static newGridItem(uid: string) {
    return {
      ID: "",
      Name: "",
      UID: uid,
      Items: [],
      ItemType: "grid"
    } as Item;
  }
}

export class Section {
  Name: string;
  Items: Item[];
  FieldOrder: string[];
  ID: string;
  CGID: number;
  Type: number;
  IsVisible: boolean;
  PartialFile: string;
  Height: string;

  static newSection(id: string, name: string) {
    return {
      Name: name,
      Items: [],
      FieldOrder: [],
      ID: id,
      CGID: 0,
      Type: 0,
      IsVisible: true,
      PartialFile: "",
      Height: ""
    } as Section;
  }
}

export interface FlexRecord {
  [fieldName: string]: any;
}

export interface CgRecord extends FlexRecord {
  Oid?: number;
  cgLastModifiedField?: Date;
  EnteredByField?: string;
  EntryDateField?: Date;
  LastModifiedByField?: string;
  InactiveField?: boolean;
}

export interface Quantity {
  Amount?: any;
  Unit: string;
}

export interface ApiResponseWrapper {
  [className: string]: CgRecord[];
}

export const classMetaFor = async (className: string) =>
  (await api().get<ClassMetaResponse>(`meta/${className}`)).data[className];
