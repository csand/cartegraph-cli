import babar from "babar";
import { http } from "./http";
import { print } from "./print";
import accounting from "accounting";

interface VizGrouping {
  GroupName: string;
  Value: number;
  Count: number;
  ChildGroupings: VizGrouping[];
}

interface VizResponse {
  HasData: boolean;
  Groupings: VizGrouping[];
}

export const vizFor = async (options: {
  className: string;
  groupBy: string;
  aggregate: string;
  aggType: number;
}) => {
  const response = await http().post<VizResponse>(
    "/PrivateAPI/DataVisualization/ChartData",
    {
      ClassName: options.className,
      GroupByFields: [options.groupBy],
      AggregationType: options.aggType || 0,
      Aggregate: options.aggregate || "",
      Filter: "",
      GroupingType: 0,
      UnitToConvertTo: ""
    }
  );

  let { data } = response;
  let groupings = data.Groupings.slice(0, 8);

  const chartData = groupings.map((group, i) => [i, group.Value]);

  const tableData = {
    keys: groupings.map((_, i) => `Group ${i.toString()}`),
    data: [
      groupings.map(group => group.GroupName),
      groupings.map(group => accounting.formatNumber(group.Value, 2, ","))
    ]
  };
  return babar(chartData) + print(tableData.data, tableData.keys);
};
