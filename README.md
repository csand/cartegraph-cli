# Cartegraph CLI

This is a commandline interface for Cartegraph.

## Installation

- `git clone https://bitbucket.org/csand/cartegraph-cli`
- `npm install` or `yarn`

## Starting the app

- `npm start <your_server_url>`
- `yarn start <your_server_url>`

In either case, `<your server url>` should be the URL to the Cartegraph server, e.g. `http://localhost/Cartegraph/`

## Logging in

Once you've launched, you'll need to login. At the `CG > ` prompt, type `login`. You'll be prompted for your credentials.

Enter your credentials and wait for the `Authenticated.` message to appear.

#### Troubleshooting
If you can't log in, double-check your server URL. The server url is not validated, so any errors logging in are likely due to an incorrect server url

## Commands

### `me`

Outputs the user profile for the logged in user.

### `recordsets`

The `recordsets` command lists all top-level recordsets (not Libraries) in the system by Singular Name, Plural Name, and Class Name. This is useful for viewing what's installed, and getting the names of recordsets for use in other commands.


### `list <recordset> [-f|--filter <filter>] [-s|--sort <sort>]`

Get a list of records for `<recordset>`.

- Currently returns only the top 30 records.
  - No pagination

#### Filtering

Can optionally provide a filter by setting the `-f` or `--filter` flag and providing the filter string.

```
CG > list tasks --filter '[City] = "Dubuque"'
```

#### Sorting

Can optionally provide a sort by setting the `-s` or `--sort` flag and providing the sort string.

```
CG > list tasks --sort CityField:asc
```

### `show <recordset> <oid>`

View the details for a single record.


### `viz <recordset> <group_by_field> [aggregate_field [-a|--average]]`

Visualize data by `<recordset>` and `<group_by_field>`.  You can optionally supply an `aggregate_field` to perform a sum aggregation of that field. Add `-a` or `--average` if you want an average `aggregate_field` instead of a sum.


## FAQ

##### Is this a joke?
Mostly, yes. However, I'm intrigued by the idea of a keyboard-driven way of retrieving data from Cartegraph. To view a list of data in OMS today, you must (assuming you're logged in):

1. Go to the appropriate index page (e.g. `Resources`).
2. Add a layer for the desired recordset type (e.g. `Labor`).
3. Add a filter using the filtering tools in the sidebar (e.g. `[Department] = "Public Works"`).
4. Find the column you want to sort by in the grid and click it until it's sorted how you want. (e.g. `TitleField:desc`)

With this tool, you can get the data in a single command `list labor -f '[Department] = "Public Works"' -s TitleField:desc`. For a user familiar with this tool, the time-to-answer could be faster using this instead of the GUI apps.


##### What fields are displayed in the query results?
This app uses the Flex Views that you have defined for each recordset, specifically the `List View` and `Detail View`.

##### What did you use to build this?

- [Vorpal.js](http://vorpal.js.org) for the command line argument processing.
- [tty-table](https://github.com/tecfu/tty-table) for the tabular display.
- [Accounting.js](http://openexchangerates.github.io/accounting.js/) for number and currency formatting.
- [Moment.js](https://momentjs.com) for date formatting.
- [Lodash](https://lodash.com) because what doesn't use lodash?
- [Axios](https://github.com/axios/axios) for HTTP.

##### Why are the `list` results slow?
Large table rendering seems to be the bottleneck here, which is why the app is currently limited to 30 rows of data.



## Debugging

Included is a `.vscode` directory containing a `launch.json` file that's set up to do debugging in VS Code.

The `launch.json` is configured for `http://localhost/Cartegraph`, and you should replace this with your server URL.