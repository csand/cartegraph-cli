// const axios = require("axios").default;
import axios from "axios";
import axiosCookieJarSupport from "@3846masa/axios-cookiejar-support";
import tough from "tough-cookie";

axiosCookieJarSupport(axios);

const cookieJar = new tough.CookieJar();
let serverUrl = "";

export const http = (url?: string) =>
  axios.create({
    baseURL: `${serverUrl}${url || ""}`,
    jar: cookieJar, // tough.CookieJar or boolean
    withCredentials: true // If true, send cookie stored in jar
  });

export const api = () => http("/api/v1/");

export const server = (server: string) => (serverUrl = server);
