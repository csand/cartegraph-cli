import { vizFor } from "./viz";
import Vorpal = require("vorpal");
import { Args, CommandInstance } from "vorpal";
import { api, http, server } from "./http";
import { print, printMeta } from "./print";
import { flatten, flatMap, uniq, forOwn } from "lodash";
import {
  FlexView,
  ClassMeta,
  ClassMetaResponse,
  Section,
  Item,
  ApiResponseWrapper,
  classMetaFor,
  PropertyMeta
} from "./meta";
import { recordsets, findRecordset } from "./data";

const vorpal = new Vorpal();
let authenticated = false;

const serverUrl = process.argv[2];
if (!serverUrl) {
  throw "You must specify a valid Cartegraph URL";
}

server(serverUrl);

vorpal
  .command("login", "Authenticate with the server")
  .action(async function(this: CommandInstance) {
    this.log("Signing in...");
    return this.prompt(
      [
        {
          type: "input",
          name: "username",
          message: "Username:"
        },
        {
          type: "password",
          name: "password",
          message: "Password:"
        }
      ],
      async answers => {
        try {
          const resp = await api().post("authenticate", {
            username: answers["username"],
            password: answers["password"]
          });
          await recordsets(); // load the cache.
          this.log("Authenticated.");
          authenticated = true;
          addAuthorizedCommands();
        } catch (error) {
          this.log(error.response.data.Message);
        }
      }
    );
  });

function addAuthorizedCommands() {
  vorpal
    .command("me", "Get current user's information")
    .action(async function(this: CommandInstance, args: Args) {
      if (authenticated) {
        const user = await api().get("user");
        this.log(print([user.data]));
      } else {
        this.log("unauthenticated");
      }
    });

  vorpal
    .command("recordsets", "Get a list of recordsets")
    .action(async function(this: CommandInstance, args: Args) {
      if (authenticated) {
        this.log(print(await recordsets()));
      } else {
        this.log("unauthenticated");
      }
    });

  vorpal
    .command("list <recordset>", "Get a list of asset records for <recordset>")
    .option("-c, --compact", "Compact output", [])
    .option("-f, --filter <filter>", "Filter string", [])
    .option("-s, --sort <sort>", "Sort string", [])
    .action(async function(this: CommandInstance, args: Args) {
      if (authenticated) {
        try {
          const type = await findRecordset(args.recordset as string);
          const ajax = api();
          const layoutResp = await ajax.get<FlexView>(
            `layout/${type.ClassName}/List`
          );
          const meta = await classMetaFor(type.ClassName);

          const fields = [
            "Oid",
            ...layoutResp.data.Sections[0].Items[0].Items.map(x => x.ID)
          ];

          const response = await ajax.get(
            `classes/${type.ClassName}?limit=30&offset=0&fields=${fields.join(
              ","
            )}&filter=${args.options.filter || ""}&sort=${args.options.sort ||
              ""}`
          );

          this.log(
            printMeta({
              keys: fields,
              data: response.data[type.ClassName],
              meta
            })
          );
        } catch (error) {
          this.log(error);
        }
      } else {
        this.log("unauthenticated");
      }
    });

  vorpal
    .command("show <recordset> <oid>", "Show the details for a record")
    .action(async function(this: CommandInstance, args: Args) {
      if (authenticated) {
        try {
          const type = await findRecordset(args.recordset as string);
          const ajax = api();
          const meta = await classMetaFor(type.ClassName);

          const layoutResp = await ajax.get<FlexView>(
            `layout/${type.ClassName}/Detail`
          );
          const keys = uniq(
            flatMap(layoutResp.data.Sections, (x: Section) => x.Items)
              .map((x: Item) => x.ID)
              .filter(x => !!meta.Fields[x])
          );

          const data = (await ajax.get<ApiResponseWrapper>(
            `classes/${type.ClassName}/${args.oid}`
          )).data[type.ClassName];

          this.log(
            printMeta({
              keys,
              data,
              meta,
              compact: Boolean(args.options.compact)
            })
          );
        } catch (error) {
          this.log(error);
        }
      } else {
        this.log("unauthenticated");
      }
    });

  vorpal
    .command(
      "viz <recordset> <group_by> [aggregate]",
      "Visualize data for recordset"
    )
    .option("-a, --average", "Aggregate average instead of sum.", [])
    .action(async function(this: CommandInstance, args: Args) {
      if (authenticated) {
        try {
          const recordset = await findRecordset(args.recordset as string);

          if (!recordset)
            return this.log(`I don't know what "${args.recordset}" is...`);

          const classMeta = await classMetaFor(recordset.ClassName);

          const groupBy = propertyMetaFor(classMeta, args.group_by as string);

          if (!groupBy)
            if (args.group_by)
              return this.log(
                `I couldn't find a field "${args.group_by}" on ${
                  recordset.PluralName
                }...`
              );
            else return this.log(`You forgot the <group_by> parameter...`);

          const aggBy = propertyMetaFor(classMeta, args.aggregate as string);

          if (!aggBy)
            if (args.aggregate)
              return this.log(
                `I couldn't find a field "${args.aggregate}" on ${
                  recordset.PluralName
                }...`
              );

          this.log(
            await vizFor({
              className: recordset.ClassName,
              groupBy: groupBy.Name,
              aggregate: aggBy ? aggBy.Name : "",
              aggType: aggBy ? (args.options.average ? 2 : 1) : 0
            })
          );
        } catch (error) {
          this.log(error);
        }
      } else {
        this.log("unauthenticated");
      }
    });
}

vorpal.delimiter("CG >").show();
function propertyMetaFor(classMeta: ClassMeta, fieldName: string) {
  let outField: PropertyMeta;

  if (!fieldName) return null;

  forOwn(classMeta.Fields, field => {
    if (
      field.Name.toLowerCase() === fieldName.toLowerCase() ||
      field.DisplayName.toLowerCase() === fieldName.toLowerCase() ||
      field.Uid.toLowerCase() === fieldName.toLowerCase()
    )
      outField = field;
  });

  return outField;
}
