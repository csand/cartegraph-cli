declare module "babar" {
  type BabarData = number[][];
  type BabarOptions = {
    color:
      | "yellow"
      | "cyan"
      | "white"
      | "magenta"
      | "green"
      | "red"
      | "grey"
      | "blue"
      | "ascii";
    width: number;
    height: number;
    minY: number;
    maxY: number;
    minX: number;
    maxX: number;
    xFractions: number;
    yFractions: number;
  };

  function babar(data: BabarData): string;

  export default babar;
}
