import Table from "tty-table";
import { ClassMeta, CgRecord, eFieldType, Quantity } from "./meta";
import moment from "moment";
import chalk from "chalk";
import accounting from "accounting";

const MAX = 6;

const alignForFieldType = (type: eFieldType) => {
  switch (type) {
    case eFieldType.FloatingPoint:
    case eFieldType.Integer:
    case eFieldType.Currency:
    case eFieldType.Quantity:
      return "right";
    default:
      return "center";
  }
};

export interface PrintMetaOptions {
  keys: string[];
  data: any[];
  meta: ClassMeta;
  compact?: boolean;
}

export const printMeta = (options: PrintMetaOptions) => {
  const { keys, meta, data, compact } = options;

  if (!keys.length) return "";

  const isSingle = data.length === 1;

  const header = keys.map(x => ({
    value: meta.Fields[x] ? meta.Fields[x].DisplayName : x,
    formatter: formatFieldType(
      meta.Fields[x] ? meta.Fields[x].FieldType : eFieldType.Text
    ),
    width: data.length > 1 ? null : 30,
    align:
      meta.Fields[x] && !isSingle
        ? alignForFieldType(meta.Fields[x].FieldType)
        : "center"
  }));

  if (isSingle && keys.length > MAX) {
    return (
      printMeta({ keys: keys.slice(0, MAX), data, meta }) +
      printMeta({ keys: keys.slice(MAX, keys.length - 1), data, meta })
    );
  } else {
    var t1 = Table(
      header,
      data.map((record: CgRecord) => keys.map(key => record[key]) || ""),
      null,
      {
        borderStyle: 1,
        borderColor: "blue",
        paddingBottom: 0,
        headerAlign: "center",
        align: "center",
        color: "white",
        truncate: "...",
        compact: compact === true
      }
    );

    return t1.render();
  }
};

export const print = (data: any[], keyList?: string[]) => {
  const keys = keyList || Object.keys(data[0]);

  const header = keys.map(x => ({ value: x }));

  var t1 = Table(
    header,
    keyList ? data : data.map(record => keys.map(key => record[key])),
    null,
    {
      borderStyle: 1,
      borderColor: "blue",
      paddingBottom: 0,
      headerAlign: "center",
      align: "center",
      color: "white",
      truncate: "...",
      compact: true
    }
  );

  return t1.render();
};

function formatFieldType(fieldType: eFieldType) {
  return (value: any) => {
    switch (fieldType) {
      case eFieldType.FloatingPoint:
        if (typeof value !== "number") value = 0;
        return `${((value || 0) as number).toFixed(1)}`;

      case eFieldType.Integer:
        if (typeof value !== "number") value = 0;
        return `${((value || 0) as number).toFixed(0)}`;

      case eFieldType.Currency:
        if (typeof value !== "number") value = 0;
        return accounting.formatMoney(value);

      case eFieldType.Quantity:
        const q = value as Quantity;
        return value ? `${q.Amount} ${q.Unit}` : "";

      case eFieldType.YN:
        const v = !!value ? chalk.green("yes") : chalk.rgb(129, 45, 36)("no");
        return v.toString();

      case eFieldType.Date:
      case eFieldType.DateTime:
        const invalid = value && value.includes("?");
        return !invalid
          ? moment(value || undefined).format("YYYY-MM-DD")
          : chalk.gray("<null>");
      default:
        return value ? formatText(value as string) : chalk.gray("<null>");
    }
  };
}

const formatText = (text: string) => {
  const t = text.toString().toLowerCase();

  switch (t) {
    case "important":
      return chalk.red(`${text.toUpperCase()}`);
    case "high":
      return chalk.red(text);
    case "completed":
      return chalk.green(text);
    case "canceled":
      return chalk.hex("#B92619")(text);
    default:
      return text;
  }
};
